#!/usr/bin/sh
#ubuntu 20.04
#install opensips and depencies
apt update -y && apt upgrade -y
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 049AD65B
echo "deb https://apt.opensips.org focal 3.1-releases" >/etc/apt/sources.list.d/opensips.list
echo "deb https://apt.opensips.org focal cli-nightly" >/etc/apt/sources.list.d/opensips-cli.list
apt update -y
apt-get install build-essential opensips opensips-cli opensips-regex-module -y
git clone https://erewin@bitbucket.org/yooxy/183-progress-replace.git
wget https://github.com/sippy/rtpproxy/archive/v2.1.1.tar.gz

#installing rtpproxy
tar -xf v2.1.1.tar.gz 
cd rtpproxy-2.1.1
./configure
make all
make install
#check if DAEMON path is correct in this file 
cp ../183-progress-replace/rtpproxy/systemd/rtpproxy.service /etc/systemd/system/rtpproxy.service
#cp ../183-progress-replace/rtpproxy/default/rtpproxy /etc/default
adduser --no-create-home --disabled-login rtpproxy
systemctl daemon-reload
systemctl enable rtpproxy
systemctl start rtpproxy

cd /root
mkdir /etc/opensips/tones
cp 183-progress-replace/opensips.cfg /etc/opensips
cp 183-progress-replace/access /etc/opensips
cp 183-progress-replace/ringback.wav /etc/opensips/tones
makeann /etc/opensips/tones/ringback.wav

systemctl enable opensips
systemctl start opensips





